document.querySelectorAll('a').forEach(link => {
  if (link.hash) {
    const targetElement = document.querySelector(link.hash);

    if (link.pathname === window.location.pathname) {
      link.addEventListener('click', (e) => {
        e.preventDefault();
        targetElement.scrollIntoView({
          behavior: 'smooth'
        });
      });
    }
  }
});
