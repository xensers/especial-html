const { task, watch, series, parallel, src, dest } = require('gulp'),
      browserSync = require('browser-sync').create();
      gutil       = require('gulp-util');
      ftp         = require('vinyl-ftp');

const GulpMem = require('gulp-mem')
const gulpMem = new GulpMem()

const cache  = require('gulp-cache'),
      del    = require('del');

const pug = require('gulp-pug'),
      minifyInline = require('gulp-minify-inline');

const html2pug = require('gulp-html2pug');

const imagemin    = require('gulp-imagemin'),
      imgCompress = require('imagemin-jpeg-recompress');

const svgSprite = require('gulp-svg-sprite')

const sass         = require('gulp-sass'),
      aliases      = require('gulp-style-aliases'),
      sourcemaps   = require('gulp-sourcemaps'),
      cssnano      = require('gulp-cssnano'),
      autoprefixer = require('gulp-autoprefixer');

const webpackStream = require('webpack-stream'),
      webpackConfig = require('./webpack.config.js')

const named   = require('vinyl-named');

const plumber = require('gulp-plumber'),
      notify  = require('gulp-notify'),
      errorHandler = notify.onError('<%= error.message %>');

let isDev = process.env.NODE_ENV === 'development';
gulpMem.serveBasePath = './dist';
gulpMem.logFn = null;

const paths = {
  views: {
    src: 'views/*.pug',
    dest: 'dist/',
    watch: ['views/**/*.pug', 'preloader/**/*'],
  },
  styles: {
    src: 'styles/*.scss',
    dest: 'dist/styles/',
    watch: 'styles/**/*',
  },
  scripts: {
    src: 'scripts/index.js',
    dest: 'dist/scripts/',
  },
  images: {
    src: 'images/**/*',
    dest: 'dist/images/',
    watch: 'images/**/*',
  },
  static: {
    src: 'static/**/*',
    dest: 'dist/',
    staticWatch: 'static/'
  },
  html2pug: {
    src: 'utils/html2pug/input/**/*.html',
    dest: 'utils/html2pug/output/',
    watch: 'utils/html2pug/input/**/*.html',
  }
};

function dist(directory, mem = true) {
  if (isDev && mem) return gulpMem.dest(directory);
  return dest(directory);
}

const conn = ftp.create( {
  host:     'i91900pm.beget.tech',
  user:     'i91900pm_1',
  password: 'mm1%Ptzt',
  parallel: 10,
  log:      gutil.log
} );

function deploy(globs) {
  return src( globs, { base: 'dist/', buffer: false } )
    .pipe( conn.newer( '/i91900pm.beget.tech/public_html/'))
    .pipe( conn.dest( '/i91900pm.beget.tech/public_html/'))
}

task('deploy', function () {
  return deploy([
    'dist/**'
  ]);
});

task('serve', () => {
  browserSync.init({
    server: {
      baseDir: "dist/",
      middleware: isDev ? gulpMem.middleware: null
    }
  });
});

task('scripts', cb => src(paths.scripts.src)
  .pipe(plumber({errorHandler}))
  .pipe(named())
  .pipe(webpackStream( webpackConfig ))
  .pipe(dist(paths.scripts.dest))
  .pipe(browserSync.stream())
);

task('styles', cb => src(paths.styles.src)
  .pipe(plumber({errorHandler}))
  .pipe(aliases({
    "~": "./node_modules/",
    "@": "./"
  }))
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'expanded'}))
  .pipe(autoprefixer())
  .pipe(cssnano({
      reduceIdents: false
   }))
  .pipe(sourcemaps.write('./maps'))
  .pipe(dist(paths.styles.dest))
  .pipe(browserSync.reload({ stream: true }))
);

task('views', cb => src(paths.views.src)
  .pipe(plumber({errorHandler}))
  .pipe(pug({
    pretty: true
   }))
  .pipe(minifyInline())
  .pipe(dist(paths.views.dest))
  .pipe(browserSync.stream())
);

task('images', cb => src(paths.images.src)
  .pipe(plumber({errorHandler}))
  .pipe(cache(imagemin([
    imgCompress({
      loops: 4,
      min: 70,
      max: 80,
      quality: 'high'
    }),
    imagemin.gifsicle(),
    imagemin.optipng(),
    imagemin.svgo()
  ])))
  .pipe(dist(paths.images.dest))
  .pipe(browserSync.stream())
);

const config = {
  mode: {
    css: { // Activate the «css» mode
      example: true
    }
  }
};

task('sprite', cb => src('sprite/*.svg')
  .pipe(svgSprite(config))
  .pipe(dist('out'))
);

task('static', cb => src(paths.static.src)
  .pipe(plumber({errorHandler}))
  .pipe(dist(paths.static.dest))
  .pipe(browserSync.stream())
);

task('html2pug', cb => src(paths.html2pug.src)
  .pipe(plumber({errorHandler}))
  .pipe(html2pug({ fragment: true }))
  .pipe(dist(paths.html2pug.dest, false))
);

task('html2pug', cb => src(paths.html2pug.src)
  .pipe(plumber({errorHandler}))
  .pipe(html2pug({ fragment: true }))
  .pipe(dist(paths.html2pug.dest))
);

task('watch', cb => {
  console.log('Watch for:');
  console.group();
  for (let key in paths) {
    // Assets watchers
    if (paths[key].hasOwnProperty('watch')) {
      let path = paths[key].watch;
      let watcher = watch(path, series(key));

      console.log('- ', path);

      watcher.on('all', function(stats, path) {
         console.log(`File ${path} was ${stats}`);
      });
    }

    // Static watchers
    if (paths[key].hasOwnProperty('staticWatch')) {
      let watcherStatic = watch(paths[key].src);
      let base = paths[key].staticWatch;
      console.log('- ', paths[key].src);
      watcherStatic.on('all', (stats, path) => {
        console.log(`File ${path} was ${stats}`);

        if (stats === 'add' || stats === 'change') {
          src(path, {base: base, buffer: false})
            .pipe(dist(paths[key].dest))
            .pipe(browserSync.stream())
        }
      });
    }
  }

  console.groupEnd();
  return cb();
});

task('clean', cb => del('dist'));
task('clear', cb => cache.clearAll());

task('build', parallel('views', 'scripts', 'styles', 'images', 'static'));
exports.default = series('views', parallel('watch', 'serve', 'build'));
